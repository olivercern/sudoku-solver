class SudokuField {
    private field: Array<Array<SudokuElement>>;

    public constructor() {
        this.field = new Array<Array<SudokuElement>>();
        for (let x = 0; x < 9; x++) {
            this.field[x] = new Array<SudokuElement>();
            for (let y = 0; y < 9; y++) {
                this.field[x][y] = new SudokuElement();
            }
        }
    }

    public SetElementIfFree(x: number, y: number, value: number) : Boolean {
        if (this.field[x][y].IsEmpty) {
            this.field[x][y].Value = value;
            return true;
        }
        return false;
    }

    public DebugPrint(): void {
        let output = "";
        for (let x = 0; x < 9; x++) {
            for (let y = 0; y < 9; y++) {
                output += this.field[x][y].Value + " ";
            }
            output += "\n";
        }
        console.log(output);
    }
}

class SudokuElement {
    constructor() {
        this._possibles = new Array<number>();
    }
    private _value: number = 0;
    get Value() {
        return this._value;
    }
    set Value(v: number) {
        if (v < 1 || v > 9) {
            console.log("Can't set value ${v}.");
        }
        this._value = v;
    }

    private _possibles: Array<number>;

    get Possibles() {
        return this._possibles;
    }

    public addPossible(v: number): void {
        if (!this._possibles.includes(v)) {
            this._possibles.push(v);
        } else {
            console.log("Already contains value ${v}.");
        }
    }

    get IsEmpty(): boolean {
        return this.Value == 0;
    }
}

let s = new SudokuField();
s.DebugPrint();
