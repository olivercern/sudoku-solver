enum LogType {
    Error,
    Warning,
    Info,
    Debug
}
interface ILog {
    Log(type : LogType, message : String) : void;
}

class Log implements ILog {
    private logArray : Array<{LogType, Array<string>}>;

    constructor() {
        this.logArray = new Array<{LogType, Array<string>}>();
    }
    Log(type : LogType, message : String) : void {
        this.logArray[type].push(message);
    }
}