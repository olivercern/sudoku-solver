Array.from(document.getElementsByClassName("element")).forEach(element => {
    element.addEventListener(
        "mouseover",
        function(event) {
            var id = (<HTMLElement>event.target).id;
            var x = Number(id.slice(1, 2));
            var y = Number(id.slice(2, 3));

            Array.from(document.getElementsByClassName("element")).forEach(
                e => {
                    e.classList.remove("highlight");
                }
            );

            //row
            for (var i = 0; i < 9; i++) {
                var eid = "f" + x.toString() + i.toString();
                var e = document.getElementById(eid);
                if (e != null) {
                    e.classList.add("highlight");
                }
            }
            //col
            for (var i = 0; i < 9; i++) {
                var eid = "f" + i.toString() + y.toString();
                var e = document.getElementById(eid);
                if (e != null) {
                    e.classList.add("highlight");
                }
            }
            //quadrant
            var left = Math.floor(y / 3) * 3;
            var top = Math.floor(x / 3) * 3;

            for (var ey = left; ey < left + 3; ey++) {
                for (var ex = top; ex < top + 3; ex++) {
                    let el = document.getElementById(
                        "f" + ex.toString() + ey.toString()
                    );
                    if (el != null) {
                        el.classList.add("highlight");
                    }
                }
            }
        },
        false
    );
});
